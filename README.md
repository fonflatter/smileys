# Smileys for fonflatter.de

Smileys created by Bastian Melnyk for [fonflatter.de] (released under [CC BY-NC-ND 2.0])

[fonflatter.de]: https://www.fonflatter.de/
[CC BY-NC-ND 2.0]: https://creativecommons.org/licenses/by-nc-nd/2.0/
